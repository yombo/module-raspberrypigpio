#This file was created by Yombo for use with Yombo Gateway automation
#software.  Details can be found at https://yombo.net
"""
Raspberry PI GPIO
=================

Should work on nearly any Raspberry PI model.

Provides basic access to GPIO ports. Provides timing information
between hi and low when operating as reading GPIO pins.

Allows control of setting GPIO high or low when configured as
an output pin.

License
=======

See readme.md for details.

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: Copyright 2016 by Yombo.
:license: MIT
:organization: `Yombo <https://yombo.net>`_
"""
from twisted.internet.protocol import Factory, Protocol
from twisted.internet import reactor

from yombo.modules.raspberrypigpio.txgpio.sysfs import GPIO
from yombo.core.log import get_logger
from yombo.core.module import YomboModule

logger = get_logger('modules.raspberrypigpio')


class RaspberryPiGPIO(YomboModule):
    """
    Raspbery PI GPIO.
    """

    def _init_(self, **kwargs):
        self.yombo_devices = self._ModuleDevices()
        self.gpio_devices = {}
        self.input_pins = {}

        for device_id, device in self.yombo_devices.items():
            pin_number = device.device_variables['bcm_pin']['values'][0]
            self.gpio_devices[device_id] = {
                'factory': SysfsGPIOFactory(),
            }
            if device.device_type.machine_label == 'raspberry_gpio_in':
                self.gpio_devices[device_id]['type'] = 'in'
                direction = 'in'

            else:
                self.gpio_devices[device_id]['type'] = 'out'
                direction = 'out'

            # print("RPI GPIO seting up port: %s  direction: %s" % (pin_number, direction))

            self.gpio_devices[device_id]['factory'].set_parent_info(self, device_id)
            self.gpio_devices[device_id]['protocol'] = self.gpio_devices[device_id]['factory'].buildProtocol(None)
            self.gpio_devices[device_id]['direction'] = direction
            self.gpio_devices[device_id]['pin_number'] = pin_number

            self.gpio_devices[device_id]['gpio'] = GPIO(self.gpio_devices[device_id]['protocol'],
                                                        reactor=reactor,
                                                        gpio_no=pin_number,
                                                        direction=direction,
                                                        edge='both')
        # print("self.gpio_devices: %s" % self.gpio_devices)

    # def _stop_(self, **kwargs):
    #     for device_id, device in self.gpio_devices.items():
    #         device.

    def gpio_incoming(self, device_id, data):
        print("Got GPIO data: %s = %s" % (device_id, data))
        device = self.yombo_devices[device_id]
        device.set_status(
            machine_status=data,
            requested_by_default = {
                'component': self._FullName,
            },
            reported_by=self._FullName
        )

    def _device_command_(self, **kwargs):
        """
        Received a device command.

        :param kwags: Contains 'device' and 'command'.
        :return: None
        """
        device = kwargs['device']
        device_id = device.device_id
        request_id = kwargs['request_id']

        if self._is_my_device(device) is False:
            logger.warn("This module cannot handle device_type_id: {device_type_id}", device_type_id=device.device_type_id)
            return

        if self.gpio_devices[device.device_id]['direction'] != "out":
            logger.info("RPI GPIO got request to send data to an 'input'. Aborting.")
            device.device_command_failed(request_id,
                                       message=_('module.raspberrypigpio_failed', 'Tried to send output value to an input.'))
            return

        command = kwargs['command']

        if command.machine_label in ('high', 1, '1', 'close', 'on'):
            self.gpio_devices[device_id]['gpio'].writeSomeData('1')
        else:
            self.gpio_devices[device_id]['gpio'].writeSomeData('0')

        device.device_command_done(request_id, message=_('module.raspberrypigpio_done', 'GPIO set.'))

class SysfsGPIOProtocol(Protocol):
    def connectionMade(self):
        logger.debug("GPIO connection made")

    def dataReceived(self, data):
        self.factory.on_receive(int(data))
    #
    # def connectionLost(self, reason=connectionDone):
    #     logger.info("GPIO connection lost")

class SysfsGPIOFactory(Factory):
    protocol = SysfsGPIOProtocol

    def set_parent_info(self, parent, device_id):
        self._Parent = parent
        self.device_id = device_id

    def on_receive(self, data):
        self._Parent.gpio_incoming(self.device_id, data)
